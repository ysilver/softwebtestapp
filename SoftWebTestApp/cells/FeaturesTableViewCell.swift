//
//  FeaturesTableViewCell.swift
//  SoftWebTestApp
//
//  Created by Ioannis Silvestridis on 4/11/21.
//

import UIKit

class FeaturesTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func callNumber(_ sender: Any) {
        if let url = URL(string: "tel://6946684088"), UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    @IBAction func openWebView(_ sender: Any) {
        // here we fire notification to our notification center in order to load webviewcontroller on the tableview viewcontroller
        NotificationCenter.default.post(name: Notification.Name("webView"), object: nil)

    }
}
