//
//  VideoLaunchTableViewCell.swift
//  SoftWebTestApp
//
//  Created by Ioannis Silvestridis on 4/11/21.
//

import UIKit
import YouTubePlayer
class VideoLaunchTableViewCell: UITableViewCell {
    @IBOutlet var date: UILabel!
    @IBOutlet var player: YouTubePlayerView!
    
    @IBOutlet var videoCount: UILabel!
    @IBOutlet var missionName: UILabel!
    @IBOutlet var imgView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
