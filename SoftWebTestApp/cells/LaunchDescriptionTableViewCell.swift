//
//  LaunchDescriptionTableViewCell.swift
//  SoftWebTestApp
//
//  Created by Ioannis Silvestridis on 4/11/21.
//

import UIKit

class LaunchDescriptionTableViewCell: UITableViewCell {

    @IBOutlet var textViewHeight: NSLayoutConstraint!
    @IBOutlet var textView: UITextView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func showMoreAction(_ sender: UIButton) {
        //a little logic to handle textview epansion on button click
        sender.isSelected = !sender.isSelected

        if(sender.isSelected == true)
             {
            sender.setTitle("Show less", for: .normal)
            
            textViewHeight.constant = textViewHeight.constant + 100
           
             }
             else
             {
                 sender.setTitle("Show more", for: .normal)
                 textViewHeight.constant = textViewHeight.constant - 100
             }
    }
        
    
}
