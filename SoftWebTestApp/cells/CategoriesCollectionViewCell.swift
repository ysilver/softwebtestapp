//
//  CategoriesCollectionViewCell.swift
//  SoftWebTestApp
//
//  Created by Ioannis Silvestridis on 2/11/21.
//

import UIKit

class CategoriesCollectionViewCell: UICollectionViewCell {
    @IBOutlet var selectView:UIView!{
        didSet{
            selectView.layer.cornerRadius = 15.0
            selectView.layer.masksToBounds = true
            if selectView.isHidden == true{
                selectView.isHidden = false
            }
            else{
                selectView.isHidden = true
            }
        }
    }
    @IBOutlet var whiteView:UIView!{
        didSet{
            whiteView.layer.cornerRadius = 15.0
            whiteView.layer.masksToBounds = true
        }
    }
    @IBOutlet var imgView:UIImageView!
    @IBOutlet var categoriesLbl:UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectView.isHidden = true
        // Initialization code
    }

}
