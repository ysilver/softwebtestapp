//
//  AllCategoriesTableViewCell.swift
//  SoftWebTestApp
//
//  Created by Ioannis Silvestridis on 3/11/21.
//

import UIKit

class AllCategoriesTableViewCell: UITableViewCell {
    @IBOutlet var bgroundView: UIView!{
        didSet{
            bgroundView.layer.cornerRadius = 10.0
            bgroundView.layer.masksToBounds = true
        }
    }
    
    @IBOutlet var categoriesLbl: UILabel!
    @IBOutlet var imgView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
