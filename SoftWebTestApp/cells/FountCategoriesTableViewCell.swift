//
//  FountCategoriesTableViewCell.swift
//  SoftWebTestApp
//
//  Created by Ioannis Silvestridis on 2/11/21.
//

import UIKit

class FountCategoriesTableViewCell: UITableViewCell {
    @IBOutlet var borderView:UIView!{
        didSet{
            borderView.layer.borderWidth = 2.0
            borderView.layer.cornerRadius = 10.0
            borderView.layer.masksToBounds = true
        }
    }
    @IBOutlet var imgView:UIImageView!
    @IBOutlet var catLbl:UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
