//
//  LaunchesTableViewCell.swift
//  SoftWebTestApp
//
//  Created by Ioannis Silvestridis on 4/11/21.
//

import UIKit

class LaunchesTableViewCell: UITableViewCell {

    @IBOutlet var organizationLbl: UILabel!
    @IBOutlet var missionNameLbl: UILabel!
    @IBOutlet var dateLbl: UILabel!
    @IBOutlet var imgView: UIImageView!
    {
        didSet{
            imgView.layer.cornerRadius = 15.0
            imgView.layer.masksToBounds =  true
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
