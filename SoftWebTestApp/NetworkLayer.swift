//
//  NetworkLayer.swift
//  SoftWebTestApp
//
//  Created by Ioannis Silvestridis on 2/11/21.
//

import Foundation
import Alamofire
import SwiftSpinner
private let sharedNetworkLayer = NetworkLayer()
protocol returnLaunches{
    func returnLaunchesObject()
}
class NetworkLayer  :NSObject{
    class var sharedInstance: NetworkLayer{
       
           return sharedNetworkLayer
       }
    var launchesDelegate : returnLaunches? = nil
    func getLaunches(){
        let baseURL = "https://api.spacexdata.com/v3/launches?launch_year=2020"
                
      
        AF.request(baseURL, method: .get, parameters: nil, encoding: JSONEncoding.default).responseJSON { response in
                    debugPrint(response)


                    SwiftSpinner.hide(nil)

                        if let jsonData = response.data {
                            //let decoder = JSONDecoder()
                            
                            do {
                                // make sure this JSON is in the format we expect
                                if let json = try JSONSerialization.jsonObject(with: jsonData, options: []) as? [String: Any] {
                                    // try to read out a string array
                                  
                                }
                            } catch let error as NSError {
                                print("Failed to load: \(error.localizedDescription)")
                            }
                            do {
                                let decoder = JSONDecoder()
                                // here we parse the response of the server to our model
                                let launches = try decoder.decode(Launches.self, from: jsonData)
                                // we assing the model to our singleton dependency
                                DataObject.sharedInstance.launches = launches
                                self.launchesDelegate?.returnLaunchesObject()
                                print(jsonData)
    //followin is the catch statemetns for analytic error handling
                            } catch DecodingError.dataCorrupted(let context) {
                                print(context)
                            } catch DecodingError.keyNotFound(let key, let context) {
                                print("Key '\(key)' not found:", context.debugDescription)
                                print("codingPath:", context.codingPath)
                            } catch DecodingError.valueNotFound(let value, let context) {
                                print("Value '\(value)' not found:", context.debugDescription)
                                print("codingPath:", context.codingPath)
                            } catch DecodingError.typeMismatch(let type, let context) {
                                print("Type '\(type)' mismatch:", context.debugDescription)
                                print("codingPath:", context.codingPath)
                            } catch {
                                print("error: ", error)
                            }






                }
                    else{
                       SwiftSpinner.hide()
                        print("Network Error: \(String(describing: response.error))")
                        //PopUp.sharedInstance.showSimpleAlertPopUpWith(title: "Connectivity issue", message: "The Internet connection appears to be offline.")

                        // self.helloDelegate
                    }
                }

            }
}
