//
//  DataObject.swift
//  SoftWebTestApp
//
//  Created by Ioannis Silvestridis on 2/11/21.
//

private let sharedDataObject = DataObject()

class DataObject{
    var launches: Launches?
    
    class var sharedInstance: DataObject {
        
        return sharedDataObject
        
    }
}
