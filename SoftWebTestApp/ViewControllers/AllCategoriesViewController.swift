//
//  AllCategoriesViewController.swift
//  SoftWebTestApp
//
//  Created by Ioannis Silvestridis on 3/11/21.
//

import UIKit

class AllCategoriesViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    // here we set the delegate functions of tableview
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categories!.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AllCatCell") as! AllCategoriesTableViewCell
        cell.imgView.image = UIImage(named: "\(catPhotos![indexPath.row])")
        cell.categoriesLbl.text = categories![indexPath.row]
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 62.0
    }
    
    // this delegate isnt called for some reason
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "showLaunches", sender: nil)
    }
    
    var categories:[String]?
    var catPhotos:[String]?
    @IBOutlet var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.setHidesBackButton(true, animated: true)
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.allowsSelection = true
        tableView.register(UINib(nibName: "AllCategoriesTableViewCell", bundle: nil), forCellReuseIdentifier: "AllCatCell")
       // again here we hardcode all categories, they couyld have been taken  from an api instaed and populate the table view
        categories = ["Home Furnishing","Smart Home","Fashion","Electric appliances","Computers","Gadgets","Medical Equipment","Pet"]
          catPhotos = ["homefurnishing","smarthome","fashion","electricappliances","computers","gadget","medical","pet"]
        
        // because didselect row isnt called we ve bypasased by adding a gesture recognizer on table view to show segue.Anyway there isnt any data to show from thsi view controller to the next, so its ok , but shouyld be solved
        let tap: UITapGestureRecognizer =   UITapGestureRecognizer(target: self, action: #selector(showSegue))
        self.tableView.addGestureRecognizer(tap)
        
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        setGradientBackground()
       // loginBtn.layer.cornerRadius = 25.0
    }
    @objc func showSegue(){
            performSegue(withIdentifier: "showLaunches", sender: nil)
        }
    func setGradientBackground() {
        let colorTop =  UIColor(red: 0.0/255.0, green: 220.0/255.0, blue: 255.0/255.0, alpha: 1.0).cgColor
        let colorBottom = UIColor(red: 255.0/255.0, green: 0.0/255.0, blue: 255.0/255.0, alpha: 1.0).cgColor
                    
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [colorTop, colorBottom]
        gradientLayer.locations = [0.0, 1.5]
        gradientLayer.frame = self.view.bounds
                
        self.view.layer.insertSublayer(gradientLayer, at:0)
    }

    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
