//
//  WebViewController.swift
//  SoftWebTestApp
//
//  Created by Ioannis Silvestridis on 4/11/21.
//

import UIKit

class WebViewController: UIViewController {

 
    @IBOutlet var webView: UIWebView!
    override func viewDidLoad() {
        super.viewDidLoad()
        let url = URL (string: "http://www.google.com")
              let requestObj = URLRequest(url: url!)
              webView.loadRequest(requestObj)
        // Do any additional setup after loading the view.
    }

    @IBAction func closeBtn(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
