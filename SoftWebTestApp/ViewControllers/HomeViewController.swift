//
//  HomeViewController.swift
//  SoftWebTestApp
//
//  Created by Ioannis Silvestridis on 2/11/21.
//

import UIKit
import SwiftSpinner

class HomeViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, returnLaunches, UICollectionViewDelegate, UICollectionViewDataSource ,UICollectionViewDelegateFlowLayout{
    // our collection view delgates for the spacex photos and text to be displayed received from spaceX api
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if launches?.count ?? 0 == 0 {
            return 0
        }
        else{
        return launches!.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "LaunchesCell", for: indexPath) as! LaunchesCollectionViewCell
      
        //here we add a check to see whether the url is empty and might cause the app to crash when loading an invalid url, in case the url is invalid the app returns an empty cell
        let url = (launches![indexPath.row].links.missionPatch ?? "no url")
        if url == "no url"{
            return cell
        }
        cell.imgView.load(url: URL(string: "\(url)")!)
     
        cell.detailsLbl.text = "\(launches![indexPath.row].missionName)"
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
       {
           return CGSize(width: 127.0, height: 210.0)
       }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        indexOnSelect = indexPath.row
        let vc = storyboard?.instantiateViewController(withIdentifier: "VideoViewController") as! VideoViewController
        vc.selectionIndex = indexOnSelect!

        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    func returnLaunchesObject() {
        launches = DataObject.sharedInstance.launches
        collectionView.reloadData()
    }
    
    // here we set the table view delegates for our table of fount categories
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categories!.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FountCatCell") as! FountCategoriesTableViewCell
        cell.imgView.image = UIImage(named: "\(catPhotos![indexPath.row])")
        cell.catLbl.text = categories![indexPath.row]
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 72.0
    }
    @IBOutlet var collectionView:UICollectionView!
    @IBOutlet var tableView:UITableView!
    @IBOutlet var showMoreBtn:UIButton!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet var showMorebtn: UIButton!
    var indexOnSelect :Int?
    var categories:[String]?
    var catPhotos:[String]?
    var launches = DataObject.sharedInstance.launches
    var searchLaunches:Launches?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Home"
        // here we connect data source and delegates of tableview and collection view
        tableView.delegate = self
        tableView.dataSource = self
        collectionView.delegate = self
        collectionView.dataSource = self
        // Do any additional setup after loading the view.
        tableView.register(UINib(nibName: "FountCategoriesTableViewCell", bundle: nil), forCellReuseIdentifier: "FountCatCell")
        self.collectionView.register(UINib(nibName: "LaunchesCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "LaunchesCell")

        searchBar.delegate = self
        
        // we set he properties of searchbar
        if let textfield = searchBar.value(forKey: "searchField") as? UITextField {
            textfield.backgroundColor = UIColor.lightGray
        }
        if let textfield = searchBar.value(forKey: "searchField") as? UITextField {
            textfield.attributedPlaceholder = NSAttributedString(string: textfield.placeholder ?? "", attributes: [NSAttributedString.Key.foregroundColor : UIColor.white])
        }

        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        // calling the spinner and network call before the view appears
        SwiftSpinner.show("Connecting...")
        NetworkLayer.sharedInstance.launchesDelegate = self
        NetworkLayer.sharedInstance.getLaunches()
        // we set the propertyies of showmorebtn
        showMorebtn.layer.cornerRadius = 25.0
        showMorebtn.layer.masksToBounds = true
        
        setGradientBackgroundButton()
    }
    @objc func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
       
        
    }
    @objc func performSegueView(){
        performSegue(withIdentifier: "showVideos", sender: nil)
    }
    func setGradientBackgroundButton() {
        // we set the gradiant horizontal background color of btn
        let colorTop =  UIColor(red: 0.0/255.0, green: 220.0/255.0, blue: 255.0/255.0, alpha: 1.0).cgColor
        let colorBottom = UIColor(red: 255.0/255.0, green: 0.0/255.0, blue: 255.0/255.0, alpha: 1.0).cgColor
                    
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [colorBottom, colorTop]
    
        gradientLayer.startPoint = CGPoint(x: 0, y: 0)
        gradientLayer.endPoint = CGPoint(x: 1, y: 0)
        gradientLayer.frame = self.showMorebtn.bounds
                
       
        self.showMorebtn.layer.insertSublayer(gradientLayer, at: 0)
    }
    @IBAction func showMoreAction(_ sender: Any) {
        performSegue(withIdentifier: "showAllCategories", sender: nil)
    }
    // here we bypassed the delegate of cancel uisearchbar by setting a button and connecting to this iboutlet
    @IBAction func cancelBtn(_ sender: Any) {
        searchBar.text = ""
        launches = DataObject.sharedInstance.launches
        collectionView.reloadData()
        self.view.endEditing(true)
    }
    
   
    

}
extension HomeViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
       
        // here we search the missionName string array with the searchtext
         searchLaunches = launches!.filter({$0.missionName.prefix(searchText.count) == searchText})
       
        //we provide the datasource for the collection view the searched one
        launches = searchLaunches
        collectionView.reloadData()
    }
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
       // this delegate is nto working for some reason
        searchBar.text = ""
        launches = DataObject.sharedInstance.launches
        collectionView.reloadData()
        self.view.endEditing(true)
    }
}
// here provide an extension of uiimage in order to load asynchronous the url of the photos  provided by the space x links
extension UIImageView {
            func load(url: URL) {
                DispatchQueue.global().async { [weak self] in
                    if let data = try? Data(contentsOf: url) {
                        if let image = UIImage(data: data) {
                            DispatchQueue.main.async {
                                self?.image = image
                            }
                        }
                    }
                }
            }
        }
