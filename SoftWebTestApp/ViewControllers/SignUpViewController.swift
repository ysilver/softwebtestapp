//
//  SignUpViewController.swift
//  SoftWebTestApp
//
//  Created by Ioannis Silvestridis on 1/11/21.
//

import UIKit

class SignUpViewController: UIViewController, AlertReturn {
    func returnOK() {
        self.dismiss(animated: true)
    }
    
    
   
    // we declare our variables with according properties
    @IBOutlet var continueBtn: UIButton!
    @IBOutlet var nameTxt: UITextField!{
        didSet{
            let whitePalveholderTxt = NSAttributedString(string: "Name*",attributes: [NSAttributedString.Key.foregroundColor:UIColor.black])
            let borderColor = UIColor(red:0.0/255, green: 0.0/255, blue: 0.0/255, alpha: 1.0).cgColor

            nameTxt.layer.borderColor = borderColor
            nameTxt.layer.borderWidth = 1.0
            nameTxt.layer.cornerRadius = 25.0
            nameTxt.attributedPlaceholder = whitePalveholderTxt
            let paddingView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: 20))
            nameTxt.leftView = paddingView
            nameTxt.leftViewMode = .always
        }
    }
    @IBOutlet var SurnameTxt: UITextField!{
        didSet{
            let whitePalveholderTxt = NSAttributedString(string: "Surname*",attributes: [NSAttributedString.Key.foregroundColor:UIColor.black])
            let borderColor = UIColor(red: 0.0/255, green: 0.0/255, blue: 0.0/255, alpha: 1.0).cgColor

            SurnameTxt.layer.borderColor = borderColor
            SurnameTxt.layer.borderWidth = 1.0
            SurnameTxt.layer.cornerRadius = 25.0
            SurnameTxt.attributedPlaceholder = whitePalveholderTxt
            let paddingView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: 20))
            SurnameTxt.leftView = paddingView
            SurnameTxt.leftViewMode = .always
        }
    }
    @IBOutlet var AgeTxt: UITextField!{
        didSet{
            let whitePalveholderTxt = NSAttributedString(string: "Age*",attributes: [NSAttributedString.Key.foregroundColor:UIColor.black])
            let borderColor = UIColor(red: 0.0/255, green: 0.0/255, blue: 0.0/255, alpha: 1.0).cgColor

            AgeTxt.layer.borderColor = borderColor
            AgeTxt.layer.borderWidth = 1.0
            AgeTxt.layer.cornerRadius = 25.0
            AgeTxt.attributedPlaceholder = whitePalveholderTxt
            let paddingView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: 20))
            AgeTxt.leftView = paddingView
            AgeTxt.leftViewMode = .always
        }
    }
    @IBOutlet var phoneTxt: UITextField!{
        didSet{
            let whitePalveholderTxt = NSAttributedString(string: "Phone number*",attributes: [NSAttributedString.Key.foregroundColor:UIColor.black])
            let borderColor = UIColor(red: 0.0/255, green: 0.0/255, blue: 0.0/255, alpha: 1.0).cgColor

            phoneTxt.layer.borderColor = borderColor
            phoneTxt.layer.borderWidth = 1.0
            phoneTxt.layer.cornerRadius = 25.0
            phoneTxt.attributedPlaceholder = whitePalveholderTxt
            let paddingView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: 20))
            phoneTxt.leftView = paddingView
            phoneTxt.leftViewMode = .always
        }
    }
    @IBOutlet var emailTxt: UITextField!{
        didSet{
            let whitePalveholderTxt = NSAttributedString(string: "E-mail",attributes: [NSAttributedString.Key.foregroundColor:UIColor.black])
            let borderColor = UIColor(red: 0.0/255, green: 0.0/255, blue: 0.0/255, alpha: 1.0).cgColor

            emailTxt.layer.borderColor = borderColor
            emailTxt.layer.borderWidth = 1.0
            emailTxt.layer.cornerRadius = 25.0
            emailTxt.attributedPlaceholder = whitePalveholderTxt
            let paddingView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: 20))
            emailTxt.leftView = paddingView
            emailTxt.leftViewMode = .always
        }
    }
    @IBOutlet var passwordTxt: UITextField!{
        didSet{
            let whitePalveholderTxt = NSAttributedString(string: "Password*",attributes: [NSAttributedString.Key.foregroundColor:UIColor.black])
            let borderColor = UIColor(red:0.0/255, green:0.0/255, blue: 0.0/255, alpha: 1.0).cgColor

            passwordTxt.layer.borderColor = borderColor
            passwordTxt.layer.borderWidth = 1.0
            passwordTxt.layer.cornerRadius = 25.0
            passwordTxt.attributedPlaceholder = whitePalveholderTxt
            let paddingView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: 20))
            passwordTxt.leftView = paddingView
            passwordTxt.leftViewMode = .always
        }
    }
    @IBOutlet var repassTxt: UITextField!{
        didSet{
            let whitePalveholderTxt = NSAttributedString(string: "Confirm Password*",attributes: [NSAttributedString.Key.foregroundColor:UIColor.black])
            let borderColor = UIColor(red: 0.0/255, green: 0.0/255, blue: 0.0/255, alpha: 1.0).cgColor

            repassTxt.layer.borderColor = borderColor
            repassTxt.layer.borderWidth = 1.0
            repassTxt.layer.cornerRadius = 25.0
            repassTxt.attributedPlaceholder = whitePalveholderTxt
            let paddingView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: 20))
            repassTxt.leftView = paddingView
            repassTxt.leftViewMode = .always
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        continueBtn.layer.cornerRadius = 25.0
        continueBtn.layer.masksToBounds = true
        setGradientBackgroundButton()
    }
    func setGradientBackgroundButton() {
        let colorTop =  UIColor(red: 0.0/255.0, green: 220.0/255.0, blue: 255.0/255.0, alpha: 1.0).cgColor
        let colorBottom = UIColor(red: 255.0/255.0, green: 0.0/255.0, blue: 255.0/255.0, alpha: 1.0).cgColor
                    
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [colorBottom, colorTop]
    
        gradientLayer.startPoint = CGPoint(x: 0, y: 0)
        gradientLayer.endPoint = CGPoint(x: 1, y: 0)
        gradientLayer.frame = self.continueBtn.bounds
                
       
        self.continueBtn.layer.insertSublayer(gradientLayer, at: 0)
    }
    
    @IBAction func continueBtn(_ sender: Any) {
    //here we apply a small check on the textfields if empty load alertViewController with appropriate messages
        if ( nameTxt.text == "" || SurnameTxt.text == "" || AgeTxt.text == "" || phoneTxt.text == "" || emailTxt.text == "" ||
             passwordTxt.text == "" ||
             repassTxt.text == "")
        {
            let alertViewController = AlertViewController(nibName: "AlertViewController", bundle: nil)

            alertViewController.message = "Attention"
            alertViewController.messageDetails = "You need to provide all the information"
            alertViewController.alertDelegate = self
         
            self.present(alertViewController, animated: true, completion: nil)

        }
        //This caused the app to crash so for the time being we commented out the code
//        if termsSwitch.isOn == false {
//            let alertViewController = AlertViewController(nibName: "AlertViewController", bundle: nil)
//
//            alertViewController.message = "Attention"
//            alertViewController.messageDetails = "You need to accept the terms and condictions"
//            alertViewController.alertDelegate = self
//           // alertViewController.cancelBtn.isHidden = true
//            self.present(alertViewController, animated: true, completion: nil)
//        }
//        if(policySwitch.isOn == false){
//            let alertViewController = AlertViewController(nibName: "AlertViewController", bundle: nil)
//
//            alertViewController.message = "Attention"
//            alertViewController.messageDetails = "You need to accept the policy terms"
//            alertViewController.alertDelegate = self
//           // alertViewController.cancelBtn.isHidden = true
//            self.present(alertViewController, animated: true, completion: nil)
//        }
//        if mebersTeamSwitch.isOn == false{
//            let alertViewController = AlertViewController(nibName: "AlertViewController", bundle: nil)
//
//            alertViewController.message = "Attention"
//            alertViewController.messageDetails = "You need to accept policy members terms"
//            alertViewController.alertDelegate = self
//           // alertViewController.cancelBtn.isHidden = true
//            self.present(alertViewController, animated: true, completion: nil)
//        }

    }
 
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
