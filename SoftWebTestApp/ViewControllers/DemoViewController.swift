//
//  DemoViewController.swift
//  SoftWebTestApp
//
//  Created by Ioannis Silvestridis on 31/10/21.
//

import UIKit

class DemoViewController: UIViewController {
    
    @IBOutlet var loginBtn: UIButton!
    @IBOutlet var videosBtn: UIButton!
    @IBOutlet var gradientButton: UIButton!
    @IBOutlet weak var gradientView: UIView!
    override func viewDidLoad() {
        
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        // we set the properties for our variables
        gradientView.layer.cornerRadius = 25.0
        gradientView.layer.masksToBounds = true
        loginBtn.layer.cornerRadius = 15.0
        loginBtn.layer.masksToBounds = true
        gradientButton.layer.cornerRadius = 25.0
        gradientButton.layer.masksToBounds = true
        videosBtn.layer.cornerRadius = 25.0
        videosBtn.layer.masksToBounds = true
        
        videosBtn.layer.borderWidth = 1.0
        setGradientBackground()
        setGradientBackgroundButton()
    }
    func setGradientBackground() {
        // here we set the basckground color of uiview(gradientView)
        let colorTop =  UIColor(red: 0.0/255.0, green: 220.0/255.0, blue: 255.0/255.0, alpha: 1.0).cgColor
        let colorBottom = UIColor(red: 255.0/255.0, green: 0.0/255.0, blue: 255.0/255.0, alpha: 1.0).cgColor
                    
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [colorTop, colorBottom]
        gradientLayer.locations = [0.0, 1.0]
    
        gradientLayer.frame = self.gradientView.bounds
                
        self.gradientView.layer.insertSublayer(gradientLayer, at:0)
        
    }
    func setGradientBackgroundButton() {
        // here we set the horizontal background gradient btn color
        let colorTop =  UIColor(red: 0.0/255.0, green: 220.0/255.0, blue: 255.0/255.0, alpha: 1.0).cgColor
        let colorBottom = UIColor(red: 255.0/255.0, green: 0.0/255.0, blue: 255.0/255.0, alpha: 1.0).cgColor
                    
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [colorBottom, colorTop]
    
        gradientLayer.startPoint = CGPoint(x: 0, y: 0)
        gradientLayer.endPoint = CGPoint(x: 1, y: 0)
        gradientLayer.frame = self.gradientButton.bounds
                
       
        self.gradientButton.layer.insertSublayer(gradientLayer, at: 0)
    }
    @IBAction func loginBtn(_ sender: Any) {
        self.dismiss(animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
