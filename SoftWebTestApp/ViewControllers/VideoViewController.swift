//
//  VideoViewController.swift
//  SoftWebTestApp
//
//  Created by Ioannis Silvestridis on 4/11/21.
//

import UIKit

class VideoViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    // here we set the delegate functions of the table view
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // here we use four cells to populate our table(one empty and three custom cells )
  let cell = tableView.dequeueReusableCell(withIdentifier: "VideoCell") as! VideoLaunchTableViewCell
        if indexPath.row == 0{
        let cell = tableView.dequeueReusableCell(withIdentifier: "VideoCell") as! VideoLaunchTableViewCell
            cell.missionName.text = DataObject.sharedInstance.launches?[selectionIndex ?? 1].missionName
            var date = DataObject.sharedInstance.launches?[selectionIndex ?? 1].launchDateUTC
            let dateUi = date?.prefix(10)
        date = String(dateUi ?? "0/0/0000")
        cell.date.text = date
        
        let url = (DataObject.sharedInstance.launches?[selectionIndex ?? 1].links.missionPatchSmall ?? "no url")
        if url == "no url"{
            return cell
        }
        cell.imgView.load(url: URL(string: "\(url)")!)
        let myVideoURL = NSURL(string: "\(DataObject.sharedInstance.launches?[selectionIndex ?? 1].links.videoLink ?? "no video" )")
        cell.player.loadVideoURL(myVideoURL! as URL)
            cell.videoCount.text =      "\(DataObject.sharedInstance.launches!.count) launches"
        
        return cell
        }
        else if indexPath.row  ==  1{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "LaunchDescCell") as! LaunchDescriptionTableViewCell
            cell.textView.text = DataObject.sharedInstance.launches?[selectionIndex ?? 1].details
            return cell
        }
        else if indexPath.row == 2{
            let cell = tableView.dequeueReusableCell(withIdentifier: "FeaturesCell") as! FeaturesTableViewCell
            return cell
        }
    return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0{
            return 360.0}
        else {
            return 220.0
        }
    }
    var selectionIndex:Int?
    var launch: Launch?
    @IBOutlet var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "SpaceX Launch"
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: "VideoLaunchTableViewCell", bundle: nil), forCellReuseIdentifier: "VideoCell")
        tableView.register(UINib(nibName: "LaunchDescriptionTableViewCell", bundle: nil), forCellReuseIdentifier: "LaunchDescCell")
        tableView.register(UINib(nibName: "FeaturesTableViewCell", bundle: nil), forCellReuseIdentifier: "FeaturesCell")
    
        
        // here we set a notification observer to listent to notifications fired by the cell when the webview must be shown
        NotificationCenter.default.addObserver(self, selector: #selector(onDidFireWebView), name: Notification.Name("webView"), object:nil)
    }
    @objc func onDidFireWebView(){
        //a little mvvm approach
        let newViewController = WebViewController(nibName: "WebViewController", bundle: nil)

        // Present View "Modally"
        self.present(newViewController, animated: true, completion: nil)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
