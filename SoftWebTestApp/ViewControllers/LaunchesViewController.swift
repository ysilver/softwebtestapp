//
//  LaunchesViewController.swift
//  SoftWebTestApp
//
//  Created by Ioannis Silvestridis on 4/11/21.
//

import UIKit

class LaunchesViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    // we set the delegate functions of our videos tableview
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return DataObject.sharedInstance.launches!.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LaunchCell") as! LaunchesTableViewCell
        let url = (DataObject.sharedInstance.launches![indexPath.row].links.missionPatch ?? "no url")
        if url == "no url"{
            return cell
        }
        cell.imgView.load(url: URL(string: "\(url)")!)
        var date = DataObject.sharedInstance.launches![indexPath.row].launchDateUTC
        let dateUi = date.prefix(10)
        date = String(dateUi)
        cell.dateLbl.text = date
        
        cell.missionNameLbl.text = DataObject.sharedInstance.launches![indexPath.row].missionName
        cell.organizationLbl.text = "SpaceX"
        
        return cell
    }
    
    // here there is a bug when selecting row it appears you have to douyble select rows in order for the delegate to fire
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
       
        let vc = storyboard?.instantiateViewController(withIdentifier: "VideoViewController") as! VideoViewController
        vc.selectionIndex = indexPath.row

        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBOutlet var launchCntLbl: UILabel!
    @IBOutlet var tableView:UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = " SpaceX Videos"
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: "LaunchesTableViewCell", bundle: nil), forCellReuseIdentifier: "LaunchCell")
        launchCntLbl.text = "\(DataObject.sharedInstance.launches!.count) launches"
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
