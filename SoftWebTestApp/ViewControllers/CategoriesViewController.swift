//
//  CategoriesViewController.swift
//  SoftWebTestApp
//
//  Created by Ioannis Silvestridis on 2/11/21.
//

import UIKit

class CategoriesViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    // our collection view delgates
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return categories!.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CategoriesCell", for: indexPath) as! CategoriesCollectionViewCell
        cell.categoriesLbl?.text = categories![indexPath.row]
        cell.imgView?.image = UIImage(named:"\(categoriesPhotos![indexPath.row])")
          return cell
        
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as! CategoriesCollectionViewCell
        
        if cell.selectView.isHidden == true{
            cell.selectView.isHidden = false
            //indexNo = indexNo! + 1
            selectedCats?.append(categories![indexPath.row])
            selectedPhotos?.append(categoriesPhotos![indexPath.row])
        }
        else{
            // here there is a problem with the logic as the following only removes last object of array and not an intermediate one for erxample
            cell.selectView.isHidden = true
            selectedCats?.removeLast()
            selectedPhotos?.removeLast()
        }
    }
        
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
       {
          return CGSize(width: 143.0, height: 169.0)
       }
    
    @IBOutlet var collectionView:UICollectionView!
    @IBOutlet var confirmButton:UIButton!
    var categories:[String]?
    var categoriesPhotos:[String]?
    var selectedCats:[String]? = []
    var selectedPhotos:[String]? = []
    var indexNo:Int? = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        // here the categories and photo names are hard coded they could be taken from an API
      categories = ["Home Furnishing","Smart Home","Fashion","Electric appliances","Computers","Gadgets","Medical Equipment"]
        categoriesPhotos = ["homefurnishing","smarthome","fashion","electricappliances","computers","gadget","medical"]
        collectionView.delegate = self
        collectionView.dataSource = self
    
        self.collectionView.register(UINib(nibName: "CategoriesCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "CategoriesCell")


        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        confirmButton.layer.cornerRadius = 25.0
        confirmButton.layer.masksToBounds = true
        setGradientBackgroundButton()
    }
    func setGradientBackgroundButton() {
        let colorTop =  UIColor(red: 0.0/255.0, green: 220.0/255.0, blue: 255.0/255.0, alpha: 1.0).cgColor
        let colorBottom = UIColor(red: 255.0/255.0, green: 0.0/255.0, blue: 255.0/255.0, alpha: 1.0).cgColor
                    
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [colorBottom, colorTop]
    
        gradientLayer.startPoint = CGPoint(x: 0, y: 0)
        gradientLayer.endPoint = CGPoint(x: 1, y: 0)
        gradientLayer.frame = self.confirmButton.bounds
                
       
        self.confirmButton.layer.insertSublayer(gradientLayer, at: 0)
    }
    
    @IBAction func confirmAction(_ sender: Any) {
       /* let vc = storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
        vc.categories = selectedCats
        vc.catPhotos = selectedPhotos
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)*/
        
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "showHome") {
              // pass data to next view
          
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        let destinationNavigationController = segue.destination as! UINavigationController
            destinationNavigationController.modalPresentationStyle = .fullScreen
        let targetController = destinationNavigationController.topViewController as! HomeViewController
       // targetController.modalPresentationStyle = .fullScreen
        targetController.categories = selectedCats
        targetController.catPhotos = selectedPhotos
        }
    }
    

}

