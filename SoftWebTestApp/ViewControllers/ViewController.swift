//
//  ViewController.swift
//  SoftWebTestApp
//
//  Created by Ioannis Silvestridis on 30/10/21.
//

import UIKit

class ViewController: UIViewController, AlertReturn {
    func returnOK() {
        self.dismiss(animated: true, completion: nil)
    }
    

   // we declare our variables with the according properties
    
    var constrainORiginalValue:CGFloat?
    @IBOutlet var donbtHaveAccLbl: UILabel!{
        didSet{
            donbtHaveAccLbl.layer.cornerRadius = 25.0
            donbtHaveAccLbl.clipsToBounds = true

        }
        
    }
    @IBOutlet var registerBtn: UIButton!{
        didSet{



          
     
            registerBtn.layer.cornerRadius = 20.0
            registerBtn.clipsToBounds = true
         
            
        
        }
    }
    @IBOutlet var loginBtn: UIButton!{
        didSet{



          
 
            loginBtn.layer.cornerRadius = 25.0
            loginBtn.clipsToBounds = true
           
            
        
        }
    }
    @IBOutlet var passwordTxt: UITextField!{
        
        didSet{
            let whitePalveholderTxt = NSAttributedString(string: "Password",attributes: [NSAttributedString.Key.foregroundColor:UIColor.white])
            
            let borderColor = UIColor(red: 255.0/255, green: 255.0/255, blue: 255.0/255, alpha: 1.0).cgColor

            passwordTxt.layer.borderColor = borderColor
            passwordTxt.layer.borderWidth = 1.0
            passwordTxt.layer.cornerRadius = 25.0
            passwordTxt.attributedPlaceholder = whitePalveholderTxt
            
            let paddingView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: 20))
            passwordTxt.leftView = paddingView
            passwordTxt.leftViewMode = .always
        }
    }
    @IBOutlet var emailTxt: UITextField!{
        didSet{
            let whitePalveholderTxt = NSAttributedString(string: "E-mail",attributes: [NSAttributedString.Key.foregroundColor:UIColor.white])
            let borderColor = UIColor(red: 255.0/255, green: 255.0/255, blue: 255.0/255, alpha: 1.0).cgColor

            emailTxt.layer.borderColor = borderColor
            emailTxt.layer.borderWidth = 1.0
            emailTxt.layer.cornerRadius = 25.0
            emailTxt.attributedPlaceholder = whitePalveholderTxt
            let paddingView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: 20))
            emailTxt.leftView = paddingView
            emailTxt.leftViewMode = .always
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
      
        
        // we add a tap gesture recognizer to detect tap on view in order to dismiss keyboard
        let tap: UITapGestureRecognizer =   UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        self.view.addGestureRecognizer(tap)
    }
    
        
      
      
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        setGradientBackground()
        loginBtn.layer.cornerRadius = 25.0
    }
    func setGradientBackground() {
        // here we set the basckground color of uiview
        let colorTop =  UIColor(red: 0.0/255.0, green: 220.0/255.0, blue: 255.0/255.0, alpha: 1.0).cgColor
        let colorBottom = UIColor(red: 255.0/255.0, green: 0.0/255.0, blue: 255.0/255.0, alpha: 1.0).cgColor
                    
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [colorTop, colorBottom]
        gradientLayer.locations = [0.0, 1.0]
        gradientLayer.frame = self.view.bounds
                
        self.view.layer.insertSublayer(gradientLayer, at:0)
    }
   
    @objc func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
      
        
    }

    @IBAction func loginAction(_ sender: Any) {
        if emailTxt.text == ""{
            let alertViewController = AlertViewController(nibName: "AlertViewController", bundle: nil)

            alertViewController.message = "Attention"
            alertViewController.messageDetails = "Just write something"
            alertViewController.alertDelegate = self
           // alertViewController.cancelBtn.isHidden = true
            self.present(alertViewController, animated: true, completion: nil)
            
        }
        if passwordTxt.text ==  ""{
            let alertViewController = AlertViewController(nibName: "AlertViewController", bundle: nil)

            alertViewController.message = "Attention"
            alertViewController.messageDetails = "Just write something"
            alertViewController.alertDelegate = self
           // alertViewController.cancelBtn.isHidden = true
            self.present(alertViewController, animated: true, completion: nil)
        }
        
        performSegue(withIdentifier: "showViewCategories", sender: nil)
    }
    
}

